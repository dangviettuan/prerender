import app from "./app";
import * as puppeteer from "puppeteer";
import { serialize } from "./renderer";
import * as fetch from "node-fetch";

const PORT = 3000;
const appURL = "__YOUR_APP_URL__";
const renderURL = "127.0.0.1/render";

// Server-side rendering API
app.use("/srr", async (request, response) => {
  console.log("/srr API: ", request.query.requestURL);

  // Which bots you want to prerender.
  const bots = [
    "twitterbot",
    "facebookexternalhit",
    "linkedinbot",
    "pinterest",
    "slackbot",
    "googlebot",
    "bingbot",
    "yandexbot",
    "duckduckbot",
    "slurp"
    // TODO: Add more bot
  ];

  const userAgent = request.headers["user-agent"] as string;

  // Is request from bots?
  // TODO: Using regular expression to optimize.
  const isBot = bots.filter(bot => userAgent.toLowerCase().includes(bot))
    .length;

  const requestURL = appURL + request.url;

  if (isBot) {
    const html = await fetch(`${renderURL}?requestURL=${requestURL}`);
    const body = await html.text();
    response.send(body.toString());
  } else {
    const html = await fetch(appURL);
    const body = await html.text();

    response.send(body.toString());
  }
});

// Render API to re-use for orther project
app.use("/render", async (request, response) => {
  console.log("/render API: ", request.query.requestURL);
  const browser = await puppeteer.launch({
    headless: true,
    args: ["--no-sandbox", "--disable-setuid-sandbox"]
  });

  const requestURL = request.query.requestURL;

  const page = await browser.newPage();
  const { status, content } = await serialize(page, requestURL, false);

  response.status(status).send(content);
});

//TODO: Implement cache strategy, /recache API

app.listen(PORT, () => {
  console.log("Express server listening on port " + PORT);
});
